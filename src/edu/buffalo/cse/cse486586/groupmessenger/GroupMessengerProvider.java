package edu.buffalo.cse.cse486586.groupmessenger;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

/**
 * GroupMessengerProvider is a key-value table. Once again, please note that we do not implement
 * full support for SQL as a usual ContentProvider does. We re-purpose ContentProvider's interface
 * to use it as a key-value table.
 * 
 * Please read:
 * 
 * http://developer.android.com/guide/topics/providers/content-providers.html
 * http://developer.android.com/reference/android/content/ContentProvider.html
 * 
 * before you start to get yourself familiarized with ContentProvider.
 * 
 * There are two methods you need to implement---insert() and query(). Others are optional and
 * will not be tested.
 * 
 * @author stevko
 *
 */
public class GroupMessengerProvider extends ContentProvider {

	public static final Uri contentUri = Uri.parse("content://edu.buffalo.cse.cse486586.groupmessenger.provider");

	private MessengerDBHelper dbHelper;
	
	private static final String DBNAME = "GROUP_MESSENGER";
	
	private SQLiteDatabase sqliteDatabase = null;
	
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // You do not need to implement this.
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        // You do not need to implement this.
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
    	
    	sqliteDatabase = dbHelper.getWritableDatabase();
        sqliteDatabase.insert(MessengerDBHelper.TABLENAME, null, values);
        sqliteDatabase.close();
        
        Log.v("insert", values.toString());
        return uri;
    }

    @Override
    public boolean onCreate() {
        // If you need to perform any one-time initialization task, please do it here.
        Log.v("OnCreate","OnCreate in Provider");
    	dbHelper = new MessengerDBHelper(getContext(), DBNAME, null, 1);
//    	dbHelper.onCreate(sqliteDatabase);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {

        sqliteDatabase = dbHelper.getReadableDatabase();
        StringBuilder conditionSb = new StringBuilder(MessengerDBHelper.KEY_COLUMN);
        conditionSb.append("=?");
        String args[] = new String[1];
        args[0] = selection;
    	Cursor cursor = sqliteDatabase.query(MessengerDBHelper.TABLENAME, projection, conditionSb.toString(), args, null, null, null);
    	if(cursor!= null){
    		cursor.moveToFirst();
    	}
    	
    	sqliteDatabase.close();
        
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // You do not need to implement this.
        return 0;
    }
}
