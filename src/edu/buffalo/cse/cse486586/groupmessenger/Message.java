
package edu.buffalo.cse.cse486586.groupmessenger;

public class Message implements Comparable<Message>{
    private int avdIndex = 0;

    private int msgIndex = 0;

    private int sequenceNumber = 0;
    
    private boolean isOrder = false;

    private String message = null;

    public Message(int avdIndex, int msgIndex, boolean isOrder, String message) {
        this.avdIndex = avdIndex;
        this.msgIndex = msgIndex;
        this.isOrder = isOrder;
        this.message = message;

    }

    public Message(String trim) {
        String[] msgComponents = trim.split("\\|\\|");
        int msgType = Integer.parseInt(msgComponents[0]);
        if (msgType == 1) {
            this.isOrder = false;
            // Chat message
            // Format :: MsgType||avdIdnex||msgIndex(device)||message
           this.avdIndex = Integer.parseInt(msgComponents[1]);
           this.msgIndex = Integer.parseInt(msgComponents[2]);
           this.message = msgComponents[3];
     } else {
            this.isOrder = true;
            this.sequenceNumber = Integer.parseInt(msgComponents[1]);
            this.avdIndex = Integer.parseInt(msgComponents[2]);
            this.msgIndex = Integer.parseInt(msgComponents[3]);

            /*
             * Format : MsgType||sequenceNumber||avdIndex||msgIndex 
             */   
        }
        // The components of the constructed message is built with || . If the
        // original message contains || the message should be handled
    }

    public int getAvdIndex() {
        return avdIndex;
    }

    public int getMsgIndex() {
        return msgIndex;
    }

    public boolean getIsOrder() {
        return isOrder;
    }

    public String getMessage() {
        return message;
    }
    
    public int getSequenceNumber(){
        return sequenceNumber;
    }

    public void setAvdindex(int avdIndex) {
        this.avdIndex = avdIndex;
    }

    public void setMsgIndex(int msgIndex) {
        this.msgIndex = msgIndex;
    }

    public void setIsOrder(boolean isOrder) {
        this.isOrder = isOrder;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public void setSequenceNumber(int sequenceNumber){
        this.sequenceNumber = sequenceNumber;
    }

    public int compare(Message arg0, Message arg1) {
        if(arg0.getSequenceNumber() > arg1.getSequenceNumber()){
            return 1;
        }else if(arg0.getSequenceNumber() < arg1.getSequenceNumber()){
            return -1;
        }
        return 0;
    }

    @Override
    public int compareTo(Message arg0) {
        if(this.getSequenceNumber() > arg0.getSequenceNumber()){
            return 1;
        }else if(this.getSequenceNumber() < arg0.getSequenceNumber()){
            return -1;
        }
        return 0;
    }}