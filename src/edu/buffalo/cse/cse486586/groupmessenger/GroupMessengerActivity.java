
package edu.buffalo.cse.cse486586.groupmessenger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.view.View.OnClickListener;
import android.telephony.TelephonyManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.TextView;

/**
 * GroupMessengerActivity is the main Activity for the assignment.
 * 
 * @author stevko
 */
public class GroupMessengerActivity extends Activity {

    static final String TAG = GroupMessengerActivity.class.getSimpleName();
    
    private ContentResolver contentResolver;

    static final String[] ports = new String[] {
            "11108", "11112", "11116", "11120", "11124" 
    };

    private int counter = 0;

    private int currentAvdIndex = 0;

    private int sequenceNumber = 0;

    private int[] counterArray = new int[] {
            0, 0, 0, 0, 0
    };

    private Queue<Message> sequenceMessages = new PriorityBlockingQueue<Message>();

    private List<Message> msgQueue = new ArrayList<Message>();

    private boolean isSequencer = false;

    private Map<String, Message> msgHash = new ConcurrentHashMap<String, Message>();

    private int lastOrderSequence = -1;

    static final int SERVER_PORT = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_messenger);

        contentResolver = getContentResolver();

        TextView tv = (TextView)findViewById(R.id.textView1);
        tv.setMovementMethod(new ScrollingMovementMethod());

        TelephonyManager tel = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String portStr = tel.getLine1Number().substring(tel.getLine1Number().length() - 4);
        final String myPort = String.valueOf((Integer.parseInt(portStr) * 2));

        for (int i = 0; i < ports.length; i++) {
            if (myPort.equals(ports[i])) {
                currentAvdIndex = i;
            }
        }

        try {
            /*
             * Create a server socket as well as a thread (AsyncTask) that
             * listens on the server port. AsyncTask is a simplified thread
             * construct that Android provides. Please make sure you know how it
             * works by reading
             * http://developer.android.com/reference/android/os/AsyncTask.html
             */
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
            Log.v("ServerSocket", "ServerSocket Created");
            new ServerTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverSocket);
        } catch (IOException e) {
            /*
             * Log is a good way to debug your code. LogCat prints out all the
             * messages that Log class writes. Please read
             * http://developer.android
             * .com/tools/debugging/debugging-projects.html and
             * http://developer.android.com/tools/debugging/debugging-log.html
             * for more information on debugging.
             */
            Log.e(TAG, "Can't create a ServerSocket");
            return;
        }

        Log.v("Server Create", "Server Create Completed");
        
        final EditText editText = (EditText)findViewById(R.id.editText1);

        /*
         * Registers OnPTestClickListener for "button1" in the layout, which is
         * the "PTest" button. OnPTestClickListener demonstrates how to access a
         * ContentProvider.
         */
        findViewById(R.id.button1).setOnClickListener(
                new OnPTestClickListener(tv, getContentResolver()));

        findViewById(R.id.button4).setOnClickListener(new OnSendClickListener(editText));

        editText.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN)
                        && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    /*
                     * If the key is pressed (i.e., KeyEvent.ACTION_DOWN) and it
                     * is an enter key (i.e., KeyEvent.KEYCODE_ENTER), then we
                     * display the string. Then we create an AsyncTask that
                     * sends the string to the remote AVD.
                     */
                    String msg = editText.getText().toString() + "\n";
                    editText.setText(""); // This is one way to reset the input
                                          // box.
                    // TextView tv = (TextView) findViewById(R.id.textView1);
                    // tv.append("\t" + msg); // This is one way to
                    // display a string.
                    /*
                     * Note that the following AsyncTask uses
                     * AsyncTask.SERIAL_EXECUTOR, not
                     * AsyncTask.THREAD_POOL_EXECUTOR as the above ServerTask
                     * does. To understand the difference, please take a look at
                     * http
                     * ://developer.android.com/reference/android/os/AsyncTask
                     * .html
                     */
                    StringBuilder sb = new StringBuilder();
                    sb.append(1);
                    sb.append("||");
                    sb.append(currentAvdIndex);
                    sb.append("||");
                    sb.append(counter);
                    sb.append("||");
                    sb.append(msg);
                    String msgToSend = sb.toString();

                    counter++;
                    for (String port : ports) {
                        new ClientTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                                msgToSend, port);
                    }
                    return true;
                }
                return false;
            }
        });

        if (myPort.equals("11108")) {
            isSequencer = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_group_messenger, menu);
        return true;
    }

    private class ServerTask extends AsyncTask<ServerSocket, String, Void> {

        @Override
        protected Void doInBackground(ServerSocket... sockets) {
            ServerSocket serverSocket = sockets[0];
            BufferedReader br = null;
            try {

                while (true) {
                    Socket client = serverSocket.accept();
                    InputStream is = client.getInputStream();
                    br = new BufferedReader(new InputStreamReader(is));
                    String line = br.readLine();
                    publishProgress(line);
                }
            } catch (IOException ex) {
                Log.e(TAG, "Coonection Accept Failed");
            } finally {
                try {
                    br.close();
                } catch (IOException ex) {
                    Log.e(TAG, "Reader Close Failed");
                }
            }

            return null;
        }

        protected void onProgressUpdate(String... strings) {
            Message msg = new Message(strings[0].trim());
            if (!msg.getIsOrder()) {
                if (isSequencer) {
                    msgQueue.add(msg);
                    new Sequencer().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                int avdIndex = msg.getAvdIndex();
                int msgIndex = msg.getMsgIndex();
                String key = avdIndex + "||" + msgIndex;
                Log.v(currentAvdIndex+"",key+"Message"+msg.getMessage());
                msgHash.put(key, msg);
            } else {
            	Log.v(currentAvdIndex+"",msg.getSequenceNumber()+"||"+msg.getAvdIndex()+"||"+msg.getMsgIndex());
                sequenceMessages.add(msg);
            }
            runSequencer();

            return;
        }
    }

    private synchronized void runSequencer(){
        while (!sequenceMessages.isEmpty()) {
            Message topMessage = sequenceMessages.poll();
            int avdIndex = topMessage.getAvdIndex();
            int msgIndex = topMessage.getMsgIndex();
            int receivedSequenceNumber = topMessage.getSequenceNumber();
            if (receivedSequenceNumber == (lastOrderSequence + 1)) {
                String key = avdIndex + "||" + msgIndex;
                Message nextMsg = msgHash.get(key);
                if(nextMsg == null){
                    Log.v("Error","Message not found"+key);
                    sequenceMessages.add(topMessage);
                    break;
                }else{
                	msgHash.remove(key);
                	lastOrderSequence++;
                	String strReceived = nextMsg.getMessage();
                	ContentValues cv = new ContentValues();
                	cv.put(MessengerDBHelper.KEY_COLUMN, receivedSequenceNumber);
                	cv.put(MessengerDBHelper.VALUE_COLUMN, strReceived);
                	contentResolver.insert(GroupMessengerProvider.contentUri, cv);
                	TextView textView = (TextView)findViewById(R.id.textView1);
                	textView.append(strReceived + "\t\n");
                }
            } else {
                Log.v("Error","Order sequence violation found"+(lastOrderSequence+1) + "Found"+receivedSequenceNumber+"\n");
                sequenceMessages.add(topMessage);
                break;
            }
        }

    }

    private class ClientTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... msgs) {
            try {

                String port = msgs[1];
                Socket socket = new Socket(InetAddress.getByAddress(new byte[] {
                        10, 0, 2, 2
                }), Integer.parseInt(port));
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.write(msgs[0]);
                out.flush();
                out.close();

                socket.close();
            } catch (UnknownHostException e) {
                Log.e(TAG, "ClientTask UnknownHostException");
            } catch (IOException e) {
                Log.e(TAG, "ClientTask socket IOException");
            }

            return null;
        }

    }

    private class OnSendClickListener implements OnClickListener {
        private EditText text = null;


        public OnSendClickListener(EditText text) {
            this.text = text;
        }

        public void onClick(View v) {

            String msg = text.getText().toString();
            text.setText("");

            StringBuilder sb = new StringBuilder();
            sb.append(1);
            sb.append("||");
            sb.append(currentAvdIndex);
            sb.append("||");
            sb.append(counter);
            sb.append("||");
            sb.append(msg);
            String msgToSend = sb.toString();

            counter++;
            for (String port : ports) {
                new ClientTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, msgToSend, port);
            }
        }
    }

    private class Sequencer extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            // while (true) {
            Message msg = msgQueue.remove(0);
            processMessage(msg);
            // }
            // }
            return null;
        }

        private void processMessage(Message msg) {
            int msgIndex = msg.getMsgIndex();
            int avdIndex = msg.getAvdIndex();
            if (msgIndex > counterArray[avdIndex] + 1) {
                msgQueue.add(msg);
                msg = null;
            } else {
                counterArray[avdIndex] = counterArray[avdIndex] + 1;
                StringBuilder sb = new StringBuilder();
                sb.append(0);
                sb.append("||");
                sb.append(sequenceNumber++);
                sb.append("||");
                sb.append(avdIndex);
                sb.append("||");
                sb.append(msgIndex);
                String msgString = sb.toString();

                for (String port : ports) {
                    new ClientTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, msgString,
                            port);
                }
            }
        }
    }

    /*
     * private class SequenceComparator implements Comparator<Message>{
     * @Override public int compare(Message arg0, Message arg1) { // 
     * Auto-generated method stub if(arg0.getSequenceNumber() <
     * arg1.getSequenceNumber()){ return 1; }else if(arg0.getSequenceNumber() <
     * arg1.getSequenceNumber()){ return -1; } return 0; } }
     */
}
