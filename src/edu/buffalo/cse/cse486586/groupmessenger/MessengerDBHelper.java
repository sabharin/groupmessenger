package edu.buffalo.cse.cse486586.groupmessenger;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public final class MessengerDBHelper extends SQLiteOpenHelper{

	protected static final String TABLENAME = "MESSAGE";
	
	protected static final String KEY_COLUMN = "key";

	protected static final String VALUE_COLUMN = "value";
	
	private static final String SQL_CREATE_MAIN = "CREATE TABLE " +
		    TABLENAME +                       // Table's name
		    "(" +                           // The columns in the table
		    KEY_COLUMN + " TEXT PRIMARY KEY, " +
		    VALUE_COLUMN + " TEXT " +
		    		"DESC TEXT)";
		
	public MessengerDBHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	    Log.v("On create","Create table");
		db.execSQL(SQL_CREATE_MAIN);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    Log.v("On upgrade","Drop table");
		db.execSQL("DROP TABLE IF EXISTS"+TABLENAME);
		db.execSQL(SQL_CREATE_MAIN);
		Log.v("On upgrade","Create table");
	}

}
